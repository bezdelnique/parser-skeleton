<?php
/**
 * Created by PhpStorm.
 * User: heman
 * Date: 19.07.2018
 * Time: 00:15
 */

namespace appbackend\assets;

use yii\web\AssetBundle;


class LodashAsset extends AssetBundle
{
    public $sourcePath = '@bower/lodash';

    public $js = [
        'lodash.js',
    ];
}

