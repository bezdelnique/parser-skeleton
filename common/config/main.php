<?php
return [
    'bootstrap' => [
        'parser-skeleton',
    ],
    'aliases' => [
        '@bower' => dirname(dirname(__DIR__)) . '/vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
        '@app' => dirname(dirname(__DIR__)) . '/app',
        '@appbackend' => dirname(dirname(__DIR__)) . '/appbackend',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'assocResourceReal' => [
            'class' => 'parser\components\AssocResourceReal\AssocResourceRealFake',
        ],
        'parserRequest' => [
            'class' => 'parser\components\Request',
        ],
        'view' => [
            'class' => 'yii\web\View',
            'renderers' => [
                'twig' => [
                    'class' => 'yii\twig\ViewRenderer',
                    'cachePath' => '@app/runtime/Twig/cache',
                    // Array of twig options:
                    'options' => [
                        'auto_reload' => true,
                        'debug' => true,
                        'strict_variables' => true,
                    ],
                    'globals' => [
                        'Yii' => ['class' => '\Yii'],
                        'html' => ['class' => '\yii\helpers\Html'],
                        'url' => ['class' => '\yii\helpers\Url'],
                        'alert' => ['class' => '\yii\bootstrap\Alert'],
                        'select2' => ['class' => '\kartik\select2\Select2'],
                    ],
                    'functions' => [
                        't' => function ($category, $token) {
                            return $token;
                        },
                        // 't' => '\Yii::t',
                        // 'siteHost' => '\app\helpers\App::getSiteHost',
                    ],
                    'filters' => [
                        // 'formatPrice' => 'app\helpers\Formatter::getPricePrintable',
                        // 'formatDate' => 'app\helpers\Formatter::asDate',
                        // 'formatDateTime' => 'app\helpers\Formatter::getDateTimePrintable',
                    ],
                    'uses' => ['yii\bootstrap'],
                ],
            ],
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
    ],
    'modules' => [
        'parser-skeleton' => [
            'class' => 'parser\modules\skeleton\Module',
        ],
    ],
];

