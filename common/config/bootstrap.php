<?php
Yii::setAlias('@common', dirname(__DIR__));
Yii::setAlias('@app', dirname(dirname(__DIR__)) . '/app');
Yii::setAlias('@appbackend', dirname(dirname(__DIR__)) . '/appbackend');
Yii::setAlias('@console', dirname(dirname(__DIR__)) . '/console');
Yii::setAlias('@parser', dirname(dirname(__DIR__)) . '/parser');
Yii::setAlias('@parser-view', dirname(dirname(__DIR__)) . '/parser/modules/_common/views');

libxml_use_internal_errors(true);

