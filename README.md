# Описание
Модуль состоит из двух частей

1. Парсер и сохранение информации в БД
2. Админка ассоциации данных

Для упрощения развёртывания используется vagrant. Система контроля версий Git.


## Требования к парсеру
1. Консольные коменды сущностей разделены, если это возможно. Категории, страницы продукта, список файлов - это три разные консольные команды
2. Повторный запуск команды не приводит к удалению информации, а лишь обновляет её. Особенно это важно по хранению сущностей основного проекта
3. Все данные получаемыуе от производителя кэшируется "как есть", это позволяет перенстраивать парсер без необходимсоти повторной закачки данных
4. Чтобы парсинг данных можно было легко перенастраивать для разбора используется DOM модель документа  

```
./yii parser-amd/parse/tree
./yii parser-amd/parse/files
./yii parser-amd/parse/operation-system
```

## Требования к админке
1. Реализовать ассоциации сущностей: типы оборудования, операционные системы, типы файлов, серии продукта, продукты
2. Типы оборудования и серии на выводе сводить к дереву

## Требования к хранению данных
1. Связка с основным основного проекта сохраняется внутри сущности парсера
2. Все таблицы должны иметь поле createdAt
3. У всех таблиц должен быть первичный ключ
3. Если данные уникальные, соответвующий индекс должен быть создан

## Требования к данным
1. Извлекать нужно всю доступную информацию по серии, продукту и файлу
2. Если описание предоставлено в html его нужно сохранить с оригинальнм форматированием 

## Требования к коду
1. Ошибки приложения явные и ориентированные на человека

## Форматирование кода, наименование переменных, структура
1. Нужно следовать примеру кода данным в скелетоне. camelCase, оступ четыре пробела, едниственное число для сущностей и т.д.
2. Семантическое разделение данных и переменных производителя и основного проекта производится префексами `source`, `real`. 

## Зависимости и технический стек
* php >= 7.2
* mariadb >= 10.2
* Yii > 2.15.0

# Что уже решено в скелетоне
0. Тестовый рабочий код на примере парсера AMD
1. Организация и подключение модуля Yii
2. Обёртка для работы с DOM. Уродливая, но делающая работу с DOM удобнее. https://github.com/bezdelnique/parser-toolbox
3. Все взаимодействие с источником данных вынесено в отдельный класс: `helpers/Downloader.php`
4. Реализация массовых встаровк в БД с сохранением данных при помощи класса `EntitiyBulk`
5. В админке реализован ассоциатор для дерева категорий и остальных сущностей

## Для ознакомления поднять skeleton
```
mkdir parser-skeleton
cd parser-skeleton
git archive --remote=git@bitbucket.org:bezdelnique/parser-skeleton.git master --format=tar | tar -xv
vagrant up

vagrant ssh
mysql -uroot -Dparser -f < /app/vagrant/mysql/parser_skeleton.sql  
``` 

# Развёртывание проекта на примере AMD
*Создание проекта на базе скелетона*

```
mkdir parser-amd
cd parser-amd

git archive --remote=git@bitbucket.org:bezdelnique/parser-skeleton.git master --format=tar | tar -xv

mv parser/modules/skeleton parser/modules/amd

find parser/modules/amd -type f -print0 | xargs -0 sed -i 's/skeleton/amd/g'
find parser/modules/amd -type f -print0 | xargs -0 sed -i 's/Skeleton/Amd/g'
find common/config/main.php -type f -print0 | xargs -0 sed -i 's/skeleton/amd/g'
find common/config/main.php -type f -print0 | xargs -0 sed -i 's/Skeleton/Amd/g'
find vagrant -type f -print0 | xargs -0 sed -i 's/skeleton/amd/g'

find appbackend/views/layouts/left.php -type f -print0 | xargs -0 sed -i 's/skeleton/amd/g'

git init
git add --all
git commit
git remote add origin git@bitbucket.org:bezdelnique/parser-amd.git
git push -u origin master

vagrant up
```

*В этой точки документации проект доступен для работы.* 

SSH
```
vagrant ssh
ssh vagrant@192.168.83.137
```

Веб

* http://parser-app.local
* http://admin.parser-app.local


База данных
```
192.168.83.137:3306
user: root
pass: 12345
db: parser
```

Консольные команды парсера
```
./yii parser-skeleton/parse/tree
./yii parser-skeleton/parse/file-list
./yii parser-skeleton/parse/operation-system
```

*Внимание!* Внутри VM, в том числе и в концигурации приложения, используется root без пароля. 

Дирректория проекта на сервере `/app`

## Структура модуля 
Модуль подключается в файле ```common/config/main.php``` в секциях bootstrap и modules.

```
tree -d parser

├── components
│   └── AssocResourceReal
├── entities
│   └── FakeHardware
│       └── Series
├── helpers
│   └── Sys
└── modules
    ├── _common
    │   └── views
    │       ├── assoc
    │       └── components
    ├── migrations
    └── amd
        ├── commands
        ├── controllers
        │   └── AssocActions
        ├── __docs
        ├── entities
        │   ├── HardwareFile
        │   ├── HardwareRelFile
        │   ├── HardwareTree
        │   └── OperationSystem
        ├── helpers
        └── views
            ├── assoc
            ├── default
            └── testing
```
  

# Критерий готовности
1. Проверить конфигурацию модуля `parser/modules/amd/config.php`
2. Сделать дамп итоговой БД: `parser/modules/amd/__docs/database-prod.sql`
3. Написать список доступных команд `parser/modules/amd/__docs/commands.md`
4. Прокомитить изменения в Git

## Тестирование команд
1. Команды отрабатывают без сбоев при первичном запуске (БД пустая)
2. Команды отрабатывают без сбоев при вторичном запуске
3. Команды сохраняют данные о связах с основным проектом

## Тестирование админки ассоциаций
1. Новые связи создаются, а старые сохраняются
2. Новые сущности серий и продуктов создаются и связка сохраняется
3. Удаление ассоциации работает

## Дополнительно
1. Указать какие хелперы вне модуля были модифицированы


