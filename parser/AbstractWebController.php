<?php
/**
 * Created by PhpStorm.
 * User: heman
 * Date: 04.12.2018
 * Time: 14:54
 */

namespace parser;


use parser\components\AssocResourceReal\AssocResourceRealInterface;
use parser\components\Request;
use yii\web\Controller;


abstract class AbstractWebController extends Controller
{
    protected function _request(): Request
    {
        return \Yii::$app->parserRequest;
    }


    protected function _resourceReal(): AssocResourceRealInterface
    {
        return \Yii::$app->assocResourceReal;
    }
}

