<?php

namespace parser\entities\FakeHardware\Series;


use parser\AbstractModel;


/**
 * This is the model class for table "hardware_series".
 *
 * @property integer $id
 * @property string $name
 * @property integer $companyId
 * @property integer $hardwareTypeId
 * @property string $createdAt
 */
class FakeHardwareSeriesModel extends AbstractModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fake_hardware_series';
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'hardwareTypeId', 'companyId'], 'required'],
            [['hardwareTypeId', 'companyId'], 'integer'],
            [['createdAt'], 'safe'],
            [['name'], 'string', 'max' => 255],
            // unique
            ['name', 'unique', 'targetAttribute' => ['name', 'companyId', 'hardwareTypeId']],
        ];
    }


    static public function find(): FakeHardwareSeriesQuery
    {
        $q = new FakeHardwareSeriesQuery(get_called_class());
        $q->orderBy(static::tableName() . '.name');
        return $q;
    }
}

