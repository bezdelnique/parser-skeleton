<?php

namespace parser\entities\FakeHardware;


use parser\AbstractModel;
use parser\entities\ExceptionModel;


/**
 * This is the model class for table "hardwares".
 *
 * @property integer $id
 * @property string $name
 * @property integer $hardwareTypeId
 * @property integer $hardwareSeriesId
 * @property integer $companyId
 * @property string $createdAt
 */
class FakeHardwareModel extends AbstractModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fake_hardware';
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'hardwareTypeId', 'hardwareSeriesId', 'companyId'], 'required'],
            [['hardwareTypeId', 'hardwareSeriesId', 'companyId'], 'integer'],
            [['createdAt'], 'safe'],
            [['name'], 'string', 'max' => 255],
            // unique
            ['name', 'unique', 'targetAttribute' => ['name', 'companyId', 'hardwareTypeId']],
        ];
    }


    /**
     * @inheritdoc
     * @return FakeHardwareQuery
     */
    static public function find()
    {
        $q = new FakeHardwareQuery(get_called_class());
        return $q->orderBy(static::tableName() . '.name asc');
    }


    /*
    static public function assoc(int $id, int $hardwareSeriesId, string $sourceHardwareId): bool
    {
        $o = static::findOne([static::tableName() . '.id' => $id]);
        if (empty($o)) {
            throw new ExceptionModel(sprintf('Hardware with id: %d not found.', $id));
        }

        $o->sourceHashedId = (string)$sourceHardwareId;
        $o->hardwareSeriesId = $hardwareSeriesId;
        $o->isAssoc = 1;
        if (!$o->validate()) {
            static::raiseException($o);
        }

        return $o->save();
    }


    static public function assocReset(int $id): bool
    {
        $o = static::findOne([static::tableName() . '.id' => $id]);
        if (empty($o)) {
            throw new ExceptionModel(sprintf('Hardware with id: %d not found.', $id));
        }

        $o->sourceHashedId = (string)$id;
        $o->isAssoc = 0;
        if (!$o->validate()) {
            static::raiseException($o);
        }

        return $o->save();
    }
    */

    static public function add($data)
    {
        // $data['isAssoc'] = 1;

        $o = new self();
        $o->attributes = $data;

        $o->saveOrRaiseException();
        return $o;
    }
}

