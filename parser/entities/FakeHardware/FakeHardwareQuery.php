<?php
/**
 * Created by PhpStorm.
 * User: heman
 * Date: 20.06.17
 * Time: 18:39
 */

namespace parser\entities\FakeHardware;


use parser\AbstractQuery;


class FakeHardwareQuery extends AbstractQuery
{
    /**
     * @param null $db
     * @return FakeHardwareModel[]|array|\yii\db\ActiveRecord[]
     */
    public function all($db = null)
    {
        return parent::all($db);
    }
}

