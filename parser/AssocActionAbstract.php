<?php
/**
 * Created by PhpStorm.
 * User: heman
 * Date: 26.01.2018
 * Time: 14:20
 */

namespace parser;


use parser\components\AssocResourceReal\AssocResourceRealInterface;
use yii\base\Action;


/**
 * Simplify move methods from controller to extracted action
 */
class AssocActionAbstract extends Action
{
    public function render($view, $params = [])
    {
        return $this->_controller()->render($view, $params);
    }


    public function renderContent($string)
    {
        return $this->_controller()->renderContent($string);
    }


    public function redirect($url, $statusCode = 302)
    {
        return $this->_controller()->redirect($url, $statusCode);
    }


    public function _companyId()
    {
        return $this->_controller()->_companyId();
    }


    /**
     * @return AbstractWebController
     */
    public function _controller()
    {
        return $this->controller;
    }


    protected function _resourceReal(): AssocResourceRealInterface
    {
        return \Yii::$app->assocResourceReal;
    }
}

