<?php
/**
 * Created by PhpStorm.
 * User: heman
 * Date: 04.12.2018
 * Time: 14:54
 */

namespace parser;


use yii\helpers\Console;


abstract class AbstractConsoleController extends \yii\console\Controller
{
    public $forceWeb = false;


    public function options($actionId)
    {
        return ['forceWeb'];
    }


    public function stdoutnl(string $string = '', $a = null)
    {
        $this->stdout($string . PHP_EOL, $a);
    }


    public function error(string $string)
    {
        $this->stdout($string . PHP_EOL, Console::FG_RED);
    }


    public function warn(string $string)
    {
        $this->stdout($string . PHP_EOL, Console::FG_YELLOW);
    }


    public function info(string $string)
    {
        $this->stdout($string . PHP_EOL, Console::FG_GREEN);
    }


    public function line(string $string)
    {
        $this->stdout($string . PHP_EOL);
    }


    public function verbose(string $string)
    {
        $this->stdout($string . PHP_EOL, Console::FG_CYAN);
    }


    public function header(string $string)
    {
        $this->stdout($string . PHP_EOL, Console::BOLD);
    }


    protected function _companyNick(): string
    {
        return $this->module->params['companyNick'];
    }
}

