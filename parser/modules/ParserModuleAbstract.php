<?php
/**
 * Created by PhpStorm.
 * User: heman
 * Date: 06.12.2018
 * Time: 21:09
 */

namespace parser\modules;


use parser\ParserControllerException;
use yii\base\BootstrapInterface;
use yii\base\Module as BaseModule;


class ParserModuleAbstract extends BaseModule implements BootstrapInterface
{
    public function bootstrap($app)
    {
        // \Yii::configure($this, require __DIR__ . '/config.php');

        // Only console and backend app allowed to route
        /*
        if (!($app->id == $this->params['app-console'] or $app->id == $this->params['app-backend'])) {
            throw new ParserControllerException("appId: {$app->id} not allowed");
        }
        */
    }
}

