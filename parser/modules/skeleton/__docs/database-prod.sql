create table parser_skeleton_hardware_file
(
    name              varchar(255)                         not null comment 'Name',
    version           varchar(255)                         not null comment 'Version',
    fileSize          varchar(255)                         not null comment 'File size',
    releaseDateString varchar(255)                         not null comment 'Release date string',
    description       varchar(16384)                       not null comment 'description',
    url               varchar(1024)                        not null comment 'URL',
    urlMd5            varchar(32)                          not null comment 'Url md5'
        primary key,
    createdAt         datetime default current_timestamp() not null comment 'Creation date'
)
    charset = utf8;

create table parser_skeleton_hardware_rel_file
(
    productId           int(11) unsigned                     not null comment 'Product',
    operationSystemName varchar(255)                         not null comment 'Operation system',
    fileTypeName        varchar(255)                         not null comment 'File type',
    urlMd5              varchar(32)                          not null comment 'Url md5',
    createdAt           datetime default current_timestamp() not null comment 'Creation date',
    primary key (productId, operationSystemName, fileTypeName, urlMd5)
)
    charset = utf8;

create index productId_IDX
    on parser_skeleton_hardware_rel_file (productId);

create table parser_skeleton_hardware_tree
(
    id                   int                                  not null comment 'Id'
        primary key,
    name                 varchar(255)                         not null comment 'Name',
    parentId             int                                  not null comment 'Parent',
    isProduct            tinyint(1) unsigned default 0        not null comment 'Is product',
    realHardwareTypeId   tinyint unsigned default 0           not null comment 'Real hardware type',
    realHardwareSeriesId int(11) unsigned default 0           null comment 'Real hardware series',
    realHardwareId       int(11) unsigned default 0           not null comment 'Real hardware',
    createdAt            datetime default current_timestamp() not null comment 'Creation date'
)
    charset = utf8;

create table parser_skeleton_operation_system
(
    id                       int(11) unsigned auto_increment comment 'Id'
        primary key,
    name                     varchar(255)                         not null comment 'Name',
    realOperationSystemId    tinyint unsigned default 0           not null comment 'Real operation system',
    realOperationSystemBitId tinyint unsigned default 0           not null comment 'Real operation system bit',
    createdAt                datetime default current_timestamp() not null comment 'Creation date',
    constraint main_UNIQ
    unique (name)
)
    charset = utf8;

