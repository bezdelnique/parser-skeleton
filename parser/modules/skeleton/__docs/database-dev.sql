CREATE TABLE `parser_skeleton_operation_system` (
    `id`                       int(11)    unsigned auto_increment NOT NULL COMMENT 'Id',
    `name`                     varchar(255)                       NOT NULL COMMENT 'Name',
    `realOperationSystemId`    tinyint unsigned                   not null                     default 0 comment 'Real operation system',
    `realOperationSystemBitId` tinyint unsigned                   not null                     default 0 comment 'Real operation system bit',
    `createdAt`                DATETIME                           NOT NULL                     DEFAULT current_timestamp() COMMENT 'Creation date',
    PRIMARY KEY (`id`),
    unique main_UNIQ (name)
) ENGINE = InnoDB DEFAULT CHARSET = utf8;


CREATE TABLE `parser_skeleton_hardware_tree` (
    `id`                   int(11)           NOT NULL COMMENT 'Id',
    `name`                 varchar(255)      NOT NULL COMMENT 'Name',
    `parentId`             int(11)           NOT NULL COMMENT 'Parent',
    `realHardwareTypeId`   tinyint unsigned  not null default 0 comment 'Real hardware type',
    `realHardwareSeriesId` int(11) unsigned           DEFAULT 0 COMMENT 'Real hardware series',
    `realHardwareId`       int(11)  unsigned not null default 0 comment 'Real hardware',
    `createdAt`            datetime          NOT NULL DEFAULT current_timestamp() COMMENT 'Creation date',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8;

alter table parser_skeleton_hardware_tree
    add isProduct tinyint(1) unsigned not null default 0 comment 'Is product' after parentId;


/*
alter table parser_skeleton_hardware_tree
    drop primary key,
    add primary key (id, typeId);
*/



-- drop table parser_skeleton_hardware_file;
CREATE TABLE `parser_skeleton_hardware_file` (
    `name`              varchar(255)   NOT NULL COMMENT 'Name',
    `version`           varchar(255)   NOT NULL COMMENT 'Version',
    `fileSize`          varchar(255)   NOT NULL COMMENT 'File size',
    `releaseDateString` varchar(255)   NOT NULL COMMENT 'Release date string',
    `description`       varchar(16384) NOT NULL COMMENT 'description',
    `url`               varchar(1024)  NOT NULL COMMENT 'URL',
    `urlMd5`            varchar(32)    NOT NULL COMMENT 'Url md5',
    `createdAt`         datetime       NOT NULL DEFAULT current_timestamp() COMMENT 'Creation date',
    PRIMARY KEY (`urlMd5`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8;

-- int unsigned 4294967295

-- drop table parser_skeleton_hardware_rel_file;
CREATE TABLE `parser_skeleton_hardware_rel_file` (
    `productId`           int(11) unsigned NOT NULL COMMENT 'Product',
    `operationSystemName` varchar(255)     NOT NULL COMMENT 'Operation system',
    `fileTypeName`        varchar(255)     NOT NULL COMMENT 'File type',
    `urlMd5`              varchar(32)      NOT NULL COMMENT 'Url md5',
    `createdAt`           datetime         NOT NULL DEFAULT current_timestamp() COMMENT 'Creation date',
    PRIMARY KEY (`productId`, `operationSystemName`, `fileTypeName`, urlMd5),
    KEY `productId_IDX` (`productId`) USING BTREE
) ENGINE = InnoDB DEFAULT CHARSET = utf8;


CREATE TABLE `parser_skeleton_hardware_file_body` (
    `url`           varchar(1024)       NOT NULL COMMENT 'URL',
    `urlMd5`        varchar(32)         NOT NULL COMMENT 'URL MD5',
    `fileBodyMd5`   varchar(255)        NOT NULL COMMENT 'File body md5',
    `filePath`      varchar(1024)       NOT NULL COMMENT 'File path',
    `fileSizeBytes` bigint(20) unsigned NOT NULL COMMENT 'File size',
    `createdAt`     datetime            NOT NULL DEFAULT current_timestamp() COMMENT 'Creation date',
    PRIMARY KEY (`urlMd5`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8;

--
-- fileSizeBytes
--
alter table parser_skeleton_hardware_file
    add column fileSizeBytes bigint unsigned default 0 comment 'File size in bytes' after fileSize;

select fileSize, (REPLACE(fileSize, ' MB', '') * 1000000), fileSizeBytes
from parser_skeleton_hardware_file
where fileSize LIKE '%MB'
limit 10;

update parser_skeleton_hardware_file
set fileSizeBytes = round(REPLACE(fileSize, ' MB', '') * 1000000)
where fileSize LIKE '%MB';

--
-- file type
--
alter table parser_skeleton_hardware_file
    add column type varchar(10) after fileId;

update parser_skeleton_hardware_file
set type = 'driver';

alter table parser_skeleton_hardware_file
    modify column type varchar(10) not null comment 'File type';



