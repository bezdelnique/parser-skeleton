<?php

use yii\db\Migration;

class m181208_153635_fake_hardware extends Migration
{
    public function up()
    {
        $sql = <<<SQL
create table if not exists fake_hardware
(
	id int unsigned auto_increment comment 'ID'
		primary key,
	name varchar(255) null comment 'Название',
	hardwareTypeId bigint unsigned default 0 not null comment 'Тип оборудования',
	hardwareSeriesId bigint unsigned default 0 not null comment 'Серия оборудования',
	companyId int unsigned not null comment 'Производитель',
	createdAt datetime default current_timestamp() null comment 'Дата создания'
)
;

create index hardwareSeries_IDX
	on fake_hardware (companyId, hardwareSeriesId)
;

create table if not exists fake_hardware_series
(
	id int unsigned auto_increment comment 'ID'
		primary key,
	name varchar(255) null comment 'Название',
	companyId int unsigned default 0 not null comment 'Компания',
	hardwareTypeId int unsigned default 0 not null comment 'Тип оборудования',
	createdAt datetime default current_timestamp() null comment 'Дата создания'
)
;


SQL;
        $this->getDb()->createCommand()->setSql($sql)->execute();
    }
}

