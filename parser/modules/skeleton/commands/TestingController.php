<?php

namespace parser\modules\skeleton\commands;

use yii\console\Controller;

class TestingController extends Controller
{
    public function actionIndex($message = 'hello world from module')
    {
        echo $message . "\n";
    }
}

