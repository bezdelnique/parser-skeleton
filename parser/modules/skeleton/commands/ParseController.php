<?php

namespace parser\modules\skeleton\commands;

use parser\AbstractConsoleController;
use parser\EntityBulk;
use parser\helpers\ParserToolbox;
use parser\modules\skeleton\entities\HardwareRelFile\ParserHardwareRelFileModel;
use parser\modules\skeleton\entities\HardwareTree\ParserHardwareTreeModel;
use parser\modules\skeleton\entities\OperationSystem\ParserOperationSystemModel;
use parser\modules\skeleton\helpers\Downloader;
use yii\console\ExitCode;


class ParseController extends AbstractConsoleController
{
    private $_bulkSize = 100;


    public function actions()
    {
        $actionClassNamespace = (new \ReflectionClass($this))->getNamespaceName() . '\ParserActions';

        return [
            'files' => [
                'class' => $actionClassNamespace . '\ParseFilesAction',
            ],
        ];
    }


    public function actionTree()
    {
        $this->header('Parse tree');

        $downloader = $this->_createDownloader();
        $treeContent = $downloader->getTree();

        $childNodes = ParserToolbox::htmlToTraverseChildNodes($treeContent, '#edit-product-type');
        // VarDumper::dump($domArray);

        $classTree = new ParserHardwareTreeModel();
        $bulkClassTree = new EntityBulk($classTree);
        $bulkClassTree->setBulkSize($this->_bulkSize);
        $bulkClassTree->setModeInsertUpdateOnDuplicate(['name', 'parentId']);

        foreach ($childNodes as $element) {
            $id = $element['attrs']['value'];
            $name = trim($element['nodeValue']);
            $parentId = $element['attrs']['data-parent'];

            $bulkClassTree->addOrBulk([
                'id' => $id,
                'name' => $name,
                'parentId' => $parentId,
            ]);

            $this->stdoutnl(sprintf('%d: %s', $id, $name));
        }

        $bulkClassTree->doLast();
        $this->stdoutnl('');


        // Hack: mark products in tree
        $hardwareTreeTableName = ParserHardwareTreeModel::tableName();
        $sql = <<<"SQL"
update {$hardwareTreeTableName} p
left join {$hardwareTreeTableName} c on p.id = c.parentId
set p.isProduct = 1
where c.id is null;
        
SQL;
        \Yii::$app->db->createCommand($sql)->execute();

        return ExitCode::OK;
    }


    public function actionOperationSystem()
    {
        $this->info('Extract Operation system from relation');
        $operationSystemTableName = ParserOperationSystemModel::tableName();
        $hardwareRelFileTableName = ParserHardwareRelFileModel::tableName();
        $sql = <<<"SQL"
insert ignore into {$operationSystemTableName} (name)
select distinct operationSystemName as name from {$hardwareRelFileTableName};

SQL;
        \Yii::$app->db->createCommand($sql)->execute();
        $this->stdoutnl();

        return ExitCode::OK;
    }


    public function _createDownloader(): Downloader
    {
        $path = $this->module->getDownloadPath($this->_companyNick());
        $self = $this;
        $downloader = new Downloader($path, function (string $string) use ($self) {
            return $self->stdoutnl($string);
        });

        return $downloader;
    }
}

