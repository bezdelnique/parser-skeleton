<?php
/**
 * Created by PhpStorm.
 * User: heman
 * Date: 15.12.2018
 * Time: 16:38
 */

namespace parser\modules\skeleton\commands\ParserActions;

use parser\AbstractConsoleAction;
use parser\EntityBulk;
use parser\ExceptionController;
use parser\helpers\ParserToolbox;
use parser\modules\skeleton\commands\ParseController;
use parser\modules\skeleton\entities\HardwareFile\ParserHardwareFileModel;
use parser\modules\skeleton\entities\HardwareRelFile\ParserHardwareRelFileModel;
use parser\modules\skeleton\entities\HardwareTree\ParserHardwareTreeModel;
use yii\console\ExitCode;


class ParseFilesAction extends AbstractConsoleAction
{
    private $_bulkSize = 100;


    public function run()
    {
        $this->header('File list');
        $products = ParserHardwareTreeModel::find()
            ->andWhere(['isProduct' => 1])
            // ->andWhere(['id' => 2476])
            // ->andWhere(['id' => 3316])
            // ->andWhere(['id' => 7706])
            // ->andWhere(['id' => [7706, 3316, 2476]])
            // ->andWhere(['>', 'id', 9000])
            ->asArray()
            ->orderBy('id asc')
            ->all();

        $downloader = $this->_controller()->_createDownloader();

        $classHardwareFile = new ParserHardwareFileModel();
        $bulkHardwareFile = new EntityBulk($classHardwareFile);
        $bulkHardwareFile->setBulkSize($this->_bulkSize);
        $bulkHardwareFile->setModeInsertUpdateOnDuplicate(['name', 'version', 'description', 'fileSize', 'url', 'releaseDateString']);

        $classHardwareRelFile = new ParserHardwareRelFileModel();
        $bulkHardwareRelFile = new EntityBulk($classHardwareRelFile);
        $bulkHardwareRelFile->setBulkSize($this->_bulkSize);
        $bulkHardwareRelFile->setModeInsertIgnore();

        foreach ($products as $product) {
            $this->line($product['name']);
            $this->stdoutnl();
            $productId = $product['id'];

            $productUrlJson = json_decode($downloader->getProductUrl($productId), true);
            $productPage = $downloader->getProductPage($productId, $productUrlJson['link']);

            foreach (ParserToolbox::htmlQuery($productPage, 'details.os-group') as $i => $element) {
                $xpath = ParserToolbox::nodeToXPath($element);

                // $operationSystemName = $xpath->query($converter->toXPath('summary'))[0]->nodeValue;
                $operationSystemName = ParserToolbox::xpathQuery($xpath, 'summary')[0]->nodeValue;
                $operationSystemName = $this->_cleanString($operationSystemName);
                // $fileTypeName = $xpath->query($converter->toXPath('h3.group-title'))[0]->nodeValue;
                $fileTypeName = ParserToolbox::xpathQuery($xpath, 'h3.group-title')[0]->nodeValue;
                $fileTypeName = $this->_cleanString($fileTypeName);

                $filesInfo = ParserToolbox::xpathQuery($xpath, 'div.os-row');
                foreach ($filesInfo as $fileInfo) {
                    $url = '';
                    $description = '';
                    $data = [
                        'version' => '',
                        'fileSize' => '',
                        'releaseDateString' => '',
                        'description' => '',
                    ];

                    $fileTypeNode = $fileInfo->previousSibling;
                    if (ParserToolbox::validateNode($fileTypeNode, 'h3', 'group-title')) {
                        $fileTypeName = $fileTypeNode->nodeValue;
                        $fileTypeName = $this->_cleanString($fileTypeName);
                        // $this->stdoutnl($fileTypeName);
                    }

                    $meta = $fileInfo->childNodes[0]->childNodes[0]->childNodes[0]->childNodes;
                    $name = $this->_cleanString($meta[0]->nodeValue);
                    // $this->stdoutnl($name);

                    $fileMetas = ParserToolbox::traverseChildNodes($meta[1]);
                    foreach ($fileMetas as $i => $fileMeta) {
                        $ref = $fileMeta['childNodes'];

                        if (count($ref) < 2 and isset($ref[0]['attrs']['href'])) {
                            $url = $ref[0]['attrs']['href'];
                            continue;
                        }

                        $metaName = trim($ref[0]['nodeValue']);
                        $data = array_merge($data, $this->_extractFileMeta($metaName, $ref));
                    }


                    // description
                    if (ParserToolbox::validateNode($meta[2], 'details', 'driver-details')) {
                        if (count($meta[2]->childNodes) > 1) {
                            $description = trim(ParserToolbox::nodeChildNodesToInnerHTML($meta[2]->childNodes[1]));
                        }
                    }

                    $urlMd5 = md5($url);
                    $data = array_merge($data, [
                        'name' => $name,
                        'description' => $description,
                        'url' => $url,
                        'urlMd5' => $urlMd5,
                    ]);
                    // VarDumper::dump($data);
                    // VarDumper::dump($data); exit;

                    $dataRel = [
                        'urlMd5' => $urlMd5,
                        'productId' => $productId,
                        'operationSystemName' => $operationSystemName,
                        'fileTypeName' => $fileTypeName,
                    ];
                    // VarDumper::dump($dataRel);

                    $bulkHardwareFile->addOrBulk($data);
                    if ($bulkHardwareRelFile->addOrBulk($dataRel)) {
                        $this->_controller()->actionOperationSystem();
                    }
                    $this->line($name);
                }
            }
        }

        $bulkHardwareFile->doLast();
        $bulkHardwareRelFile->doLast();
        $this->_controller()->actionOperationSystem();

        return ExitCode::OK;
    }


    private function _cleanString(string $value): string
    {
        $value = trim($value);
        $value = preg_replace('~\s+~', ' ', $value);
        return $value;
    }


    private function _extractFileMeta(string $metaName, array $ref): array
    {
        $metaName = $this->_cleanString($metaName);
        $metaValue = $this->_cleanString($ref[1]['nodeValue']);
        $fieldValue = $metaValue;

        switch ($metaName) {
            case 'Revision Number':
                $fieldName = 'version';
                break;
            case 'File Size':
                $fieldName = 'fileSize';
                break;
            case 'Release Date':
                $fieldName = 'releaseDateString';
                $fieldValue = $ref[1]['childNodes'][0]['attrs']['datetime'];
                break;
            default:
                throw new ExceptionController("Unknown property: [{$metaName}]");
                break;
        }

        return [$fieldName => $fieldValue];
    }


    /**
     * @return ParseController
     */
    public function _controller()
    {
        return $this->controller;
    }
}



