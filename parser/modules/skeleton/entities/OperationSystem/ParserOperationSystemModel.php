<?php
/**
 * Created by PhpStorm.
 * User: heman
 * Date: 12.06.2018
 * Time: 21:11
 */

namespace parser\modules\skeleton\entities\OperationSystem;


use parser\AbstractModel;
use yii\db\ActiveQuery;


class ParserOperationSystemModel extends AbstractModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'parser_skeleton_operation_system';
    }


    /**
     * @inheritdoc
     */

    public function rules()
    {
        return [
            [['id', 'name'], 'required'],
            [['id', 'realOperationSystemId', 'realOperationSystemBitId'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }


    /**
     * @inheritdoc
     * @return ActiveQuery
     */
    static public function find(): ActiveQuery
    {
        $q = new ActiveQuery(get_called_class());

        $q->select(static::tableName() . '.*');
        $q->from(static::tableName());

        return $q->orderBy('name asc');
    }
}

