<?php
/**
 * Created by PhpStorm.
 * User: heman
 * Date: 23.03.2018
 * Time: 13:56
 */

namespace parser\modules\skeleton\entities\HardwareTree;

use parser\AbstractQuery;

class ParserHardwareTreeQuery extends AbstractQuery
{
    public function andWhereLevel(int $level)
    {
        return $this->_andWhereByField('level', $level);
    }
}

