<?php

namespace parser\modules\skeleton\entities\HardwareTree;


use parser\AbstractModel;

/**
 * @property integer $id
 * @property integer $parentId
 * @property integer $isProduct
 * @property string $name
 * @property integer $realHardwareTypeId
 * @property integer $realHardwareSeriesId
 * @property integer $realHardwareId
 * @property string $createdAt
 */
class ParserHardwareTreeModel extends AbstractModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'parser_skeleton_hardware_tree';
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'name', 'parentId'], 'required'],
            [['id', 'parentId', 'isProduct', 'realHardwareTypeId', 'realHardwareSeriesId', 'realHardwareId'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }


    static public function find()
    {
        $q = new ParserHardwareTreeQuery(get_called_class());
        $q->select(static::tableName() . '.*');
        $q->from(static::tableName());
        return $q->orderBy('name asc');
    }


    static public function findLight()
    {
        $q = new ParserHardwareTreeQuery(get_called_class());
        return $q;
    }


    public function getName(): string
    {
        return $this->name;
    }
}

