<?php
/**
 * Created by PhpStorm.
 * User: heman
 * Date: 13.06.2018
 * Time: 13:38
 */

namespace parser\modules\skeleton\entities\HardwareFile;


use parser\AbstractQuery;


class ParserHardwareFileQuery extends AbstractQuery
{

    protected $_joinHardwareFileBody;


    /*
    public function joinLeftHardwareFileBody()
    {
        if ($this->_joinHardwareFileBody == false) {
            $tableName = $this->modelClass::tableName();
            $this->leftJoin([ParserMsiHardwareFileBodyEntity::tableName() . ' file_body'], "file_body.url = {$tableName}.url");
            $this->_joinHardwareFileBody = true;
        }

        return $this;
    }


    public function andWhereFileNotDownloaded()
    {
        $this->joinLeftHardwareFileBody();
        return $this->andWhere('file_body.url is null');
    }


    public function andWhereFileNotPublish()
    {
        $this->joinLeftHardwareFileBody();
        return $this->andWhere('file_body.url is null');
    }
    */

    public function selectUrl()
    {
        $tableName = $this->modelClass::tableName();
        return $this->select([
            $tableName . '.url',
            $tableName . '.fileSize',
        ]);
    }
}

