<?php
/**
 * Created by PhpStorm.
 * User: heman
 * Date: 13.06.2018
 * Time: 13:38
 */

namespace parser\modules\skeleton\entities\HardwareFile;


use parser\AbstractModel;
use yii\db\ActiveQuery;


/**
 * @property string $name
 * @property string $version
 * @property string $fileSize
 * @property string $url
 * @property string $urlMd5
 * @property string $releaseDateString
 * @property string $description
 * @property string $createdAt
 */
class ParserHardwareFileModel extends AbstractModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'parser_skeleton_hardware_file';
    }


    public function getPrimaryKey($asArray = false)
    {
        return ['urlMd5'];
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'url'], 'required'],
            // [['name', 'version', 'fileSize', 'url', 'releaseDateString'], 'required'],
            [['name', 'version', 'fileSize', 'releaseDateString'], 'string', 'max' => 255],
            [['url'], 'string', 'max' => 1024],
            [['urlMd5'], 'string', 'max' => 32],
            [['description'], 'string', 'max' => 16384],
        ];
    }


    public function beforeSave($insert)
    {
        $this->urlMd5 = md5($this->url);
        return parent::beforeSave($insert);
    }


    /**
     * @inheritdoc
     * @return ActiveQuery
     */
    static public function find(): ParserHardwareFileQuery
    {
        $q = new ParserHardwareFileQuery(get_called_class());
        return $q;
    }
}

