<?php
/**
 * Created by PhpStorm.
 * User: heman
 * Date: 07.02.2018
 * Time: 15:08
 */

namespace parser\modules\skeleton\entities\HardwareRelFile;


use parser\AbstractModel;


class ParserHardwareRelFileModel extends AbstractModel
{
    public static function tableName()
    {
        return 'parser_skeleton_hardware_rel_file';
    }


    public function getPrimaryKey($asArray = false)
    {
        return ['urlMd5', 'productId', 'operationSystemName', 'fileTypeName'];
    }


    public function rules()
    {
        return [
            [['urlMd5', 'productId', 'operationSystemName', 'fileTypeName'], 'required'],
            [['productId'], 'integer'],
            [['operationSystemName', 'fileTypeName'], 'string', 'max' => 255],
            [['urlMd5'], 'string', 'max' => 32],
        ];
    }
}

