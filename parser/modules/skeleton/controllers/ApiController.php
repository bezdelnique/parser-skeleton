<?php

namespace parser\modules\skeleton\controllers;


use parser\AbstractWebController;
use parser\ExceptionController;
use parser\modules\skeleton\controllers\AssocActions\HardwareTreeAction;
use parser\modules\skeleton\entities\HardwareTree\ParserHardwareTreeModel;
use yii\filters\VerbFilter;


class ApiController extends AbstractWebController
{
    public $enableCsrfValidation = false;


    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'login' => ['post'],
                ],
            ],
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function beforeAction($action)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return parent::beforeAction($action);
    }


    /**
     * {@inheritdoc}
     */
    public function afterAction($action, $result)
    {
        $result = parent::afterAction($action, $result);
        $result['token'] = \Yii::$app->request->csrfToken;
        return $result;
    }


    public function actionSeries()
    {
        // $companyId = $this->_request()->getIntOrException('realCompanyId');
        $realHardwareTypeId = $this->_request()->getIntOrException('realHardwareTypeId');

        $hardwareSeries = $this->_resourceReal()->getHardwareSeriesItems($this->_companyId(), ['hardwareTypeId' => $realHardwareTypeId]);

        $hardwareSeriesData = array_map(function ($item) {
            return [
                'id' => $item['id'],
                'name' => $item['name'],
            ];
        }, $hardwareSeries);


        return [
            'result' => 'success',
            'data' => [
                'items' => $hardwareSeriesData,
                '_meta' => [
                ],
            ],
        ];
    }


    public function actionSeriesSave()
    {
        $realCompanyId = $this->_request()->postIntOrException('companyId');
        $realHardwareTypeId = $this->_request()->postIntOrException('hardwareTypeId');
        $sourceId = $this->_request()->postIntOrException('sourceId');
        $realHardwareSeriesId = $this->_request()->postIntOrException('realHardwareSeriesId');
        $action = $this->_request()->postOrException('action');

        $sourceLeaf = ParserHardwareTreeModel::findOne(['id' => $sourceId]);
        if (empty($sourceLeaf)) {
            throw new ExceptionController(sprintf('Entity with id %d not found', $sourceId));
        }


        switch ($action) {
            case 'create':
                $realHardwareSeries = $realHardwareSeries = $this->_resourceReal()->createHardwareSeries(
                    $realCompanyId,
                    $realHardwareTypeId,
                    $sourceLeaf->getName()
                );

                $realHardwareSeriesId = $realHardwareSeries['id'];
                $realHardwareSeriesName = $realHardwareSeries['name'];
                break;
            case 'save':
            default:
                $realHardwareSeriesName = HardwareTreeAction::C_hardwareSeriesNoRel;
                if ($realHardwareSeriesId > 0) {
                    $realHardwareSeries = $this->_resourceReal()->getHardwareSeriesOrException($realCompanyId, $realHardwareSeriesId);
                    $realHardwareSeriesName = $realHardwareSeries['name'];
                }
                break;

        }

        $sourceLeaf->realHardwareSeriesId = $realHardwareSeriesId;
        $sourceLeaf->saveOrRaiseException();

        return [
            'result' => 'success',
            'data' => [
                'realHardwareSeriesId' => $realHardwareSeriesId,
                'realHardwareSeriesName' => $realHardwareSeriesName,
                '_reflection' => \Yii::$app->request->post(),
            ],
        ];
    }


    public function _companyId(): int
    {
        return $this->module->params['companyId'];
    }
}

