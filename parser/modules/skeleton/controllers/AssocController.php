<?php

namespace parser\modules\skeleton\controllers;

use parser\AbstractWebController;


class AssocController extends AbstractWebController
{
    public function init()
    {
        \Yii::$app->view->params['companyName'] = $this->module->params['companyName'];
        \Yii::$app->view->params['assocMenu'] = [
            [
                'nick' => 'operation-system',
                'name' => 'Операционные системы',
            ],
            [
                'nick' => 'hardware-type',
                'name' => 'Типы оборудования',
            ],
            [
                'nick' => 'hardware-tree',
                'name' => 'Дерево оборудования',
            ],
            [
                'nick' => 'hardware',
                'name' => 'Оборудование',
            ],
        ];

        parent::init();
    }


    /**
     * @inheritdoc
     */
    public function actions()
    {
        $actionClassNamespace = (new \ReflectionClass($this))->getNamespaceName() . '\AssocActions';

        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
            'operation-system' => [
                'class' => $actionClassNamespace . '\OperationSystemAction',
            ],
            'hardware-type' => [
                'class' => $actionClassNamespace . '\HardwareTypeAction',
            ],
            'hardware-tree' => [
                'class' => $actionClassNamespace . '\HardwareTreeAction',
            ],
            'hardware' => [
                'class' => $actionClassNamespace . '\HardwareAction',
            ],
        ];
    }


    public function actionIndex()
    {
        return $this->redirect(['hardware-tree']);
    }


    public function _companyId(): int
    {
        return $this->module->params['companyId'];
    }
}

