<?php
/**
 * Created by PhpStorm.
 * User: heman
 * Date: 07.02.2018
 * Time: 21:07
 */

namespace parser\modules\skeleton\controllers\AssocActions;


use parser\AbstractAssocAction;
use parser\helpers\Sys\ArrayHelper;
use parser\modules\skeleton\entities\HardwareTree\ParserHardwareTreeModel;
// use yii\helpers\ArrayHelper;
use yii\helpers\Url;


class HardwareTreeAction extends AbstractAssocAction
{
    const C_hardwareSeriesNoRel = 'Без связи';

    private $_statSourceHardwareAmountWithAssoc = [];
    private $_statSourceHardwareAmount = [];


    public function run()
    {
        $treeAll = ParserHardwareTreeModel::find()
            ->andWhere(['isProduct' => 0])
            ->asArray()
            ->all();
        $tree = ArrayHelper::parentBy($treeAll, 'parentId');

        $statHardwareAmountQuery = ParserHardwareTreeModel::findLight()
            ->select('count(*) as hardwareAmount, parentId')
            ->andWhere(['isProduct' => 1])
            ->asArray()
            ->groupBy('parentId');
        $sourceHardwareStatFlat = $statHardwareAmountQuery->all();
        $this->_statSourceHardwareAmount = ArrayHelper::map($sourceHardwareStatFlat, 'parentId', 'hardwareAmount');

        $statSourceHardwareWithAssocStatFlat = $this->_resourceReal()->joinHardware(
            $statHardwareAmountQuery,
            ParserHardwareTreeModel::tableName(),
            $this->_companyId()
        )->all();
        $this->_statSourceHardwareAmountWithAssoc = ArrayHelper::map($statSourceHardwareWithAssocStatFlat, 'parentId', 'hardwareAmount');

        $realHardwareSeriesMapped = $this->_getRealHardwareSeriesMapped();


        $level = 0;
        ob_start();
        echo '<ul>';
        foreach ($tree[0] as $element) {
            $elementId = $element['id'];
            $hardwareTypeId = $element['id'];
            $realHardwareTypeId = $element['realHardwareTypeId'];
            $hasChildren = isset($tree[$elementId]);

            echo "<li data-hardware-type-id='{$realHardwareTypeId}'>";
            echo $level . ': ';
            echo "<b>{$element['name']}</b>";
            echo " ({$element['id']})";
            if (isset($this->_statSourceHardwareAmount[$element['id']]) and $realHardwareTypeId) {
                echo " [hardwareAmount: {$this->_statSourceHardwareAmount[$element['id']]}]";
            }

            echo " realHardwareTypeId: {$realHardwareTypeId}";


            if ($hasChildren) {
                $this->_treeChildren($tree, $realHardwareSeriesMapped, $elementId, $hardwareTypeId, $realHardwareTypeId, $level);
            }
            echo '</li>';
        }
        echo '</ul>';

        $content = ob_get_contents();
        ob_end_clean();

        return $this->render('/assoc/tree.twig', [
            'sectionName' => 'Дерево оборудования',
            'content' => $content,
            'companyId' => $this->_companyId(),
        ]);

    }


    private function _treeChildren(array &$tree, array &$hardwareSeriesMapped, int $parentId, int $hardwareTypeId, int $realHardwareTypeId, int $level)
    {
        $level++;
        switch ($level) {
            case 1:
                $backgroundColor = '#FCF2B8';
                break;
            case 2:
                $backgroundColor = '#D5FCB8';
                break;
            case 3:
                $backgroundColor = '#B8FCFB';
                break;
            case 4:
                $backgroundColor = '#B8C0FC';
                break;
            default:
                $backgroundColor = '#E06FF7';
                break;
        }

        echo '<ul style="background-color: ' . $backgroundColor . ';">';
        foreach ($tree[$parentId] as $element) {
            $elementId = $element['id'];
            $elementName = $element['name'];
            $realHardwareSeriesId = $element['realHardwareSeriesId'];
            // $realHardwareSeriesId = 0;
            $hasChildren = isset($tree[$elementId]);

            echo "<li>";
            echo $level . ': ';
            echo "{$elementName}";
            echo " ({$elementId})";
            if (isset($this->_statSourceHardwareAmount[$elementId])) {
                $hardwareAmount = $this->_statSourceHardwareAmount[$elementId];

                // hardwareTypeId
                if ($realHardwareTypeId and $realHardwareSeriesId) {
                    $hardwareAmountAssoc = $this->_statSourceHardwareAmountWithAssoc[$elementId] ?? 0;

                    $colorName = 'red';
                    if ($hardwareAmount == $hardwareAmountAssoc) {
                        $colorName = 'grey';
                    }

                    $template = ' <a href="%s?sourceHardwareTypeId=%d&sourceHardwareSeriesId=%d" target="_blank" style="color: %s;">[&nbsp; hardwareAmount: %d of %d &nbsp;]</a>';
                    printf($template, Url::to(['hardware']), $hardwareTypeId, $elementId, $colorName, $hardwareAmountAssoc, $hardwareAmount);
                } else {
                    echo " [&nbsp; hardwareAmount: {$hardwareAmount} &nbsp;]";
                }


                if ($realHardwareTypeId and !$hasChildren) {
                    $assocName = self::C_hardwareSeriesNoRel;
                    if ($realHardwareSeriesId) {
                        $assocName = $hardwareSeriesMapped[$realHardwareSeriesId];
                    }

                    // echo " realHardwareSeriesId: {$realHardwareSeriesId}";
                    echo " <span class='assoc-js' 
                         data-hardware-type-id='{$realHardwareTypeId}' 
                         data-source-id='{$elementId}' 
                         data-real-hardware-series-id='{$realHardwareSeriesId}'>{$assocName} ({$realHardwareSeriesId})</span>
                     ";
                    echo " <span></span>";
                }
            }

            if ($hasChildren) {
                $this->_treeChildren($tree, $hardwareSeriesMapped, $elementId, $hardwareTypeId, $realHardwareTypeId, $level);
            }
            echo '</li>';
        }
        echo '</ul>';
    }


    private function _getRealHardwareSeriesMapped(): array
    {
        $hardwareSeries = $this->_resourceReal()->getHardwareSeriesItems($this->_companyId());
        return ArrayHelper::map($hardwareSeries, 'id', 'name');
    }
}

