<?php
/**
 * Created by PhpStorm.
 * User: heman
 * Date: 06.12.2018
 * Time: 14:01
 */

namespace parser\modules\skeleton\controllers\AssocActions;


use parser\AbstractAssocAction;
use parser\modules\skeleton\entities\OperationSystem\ParserOperationSystemModel;
use yii\helpers\ArrayHelper;


class OperationSystemAction extends AbstractAssocAction
{
    public function run()
    {
        if (\Yii::$app->request->isPost) {
            $assocOperationSystemData = \Yii::$app->request->post('assocOperationSystem');
            $assocOperationSystemBitData = \Yii::$app->request->post('assocOperationSystemBit');
            foreach ($assocOperationSystemData as $sourceId => $realOperationSystemId) {
                if (empty($realOperationSystemId)) {
                    continue;
                }

                if (empty($assocOperationSystemBitData[$sourceId])) {
                    continue;
                }

                $realOperationSystemBitId = $assocOperationSystemBitData[$sourceId];
                ParserOperationSystemModel::updateAll([
                    'realOperationSystemId' => $realOperationSystemId,
                    'realOperationSystemBitId' => $realOperationSystemBitId,
                ], ['id' => $sourceId]);
            }

            \Yii::$app->getSession()->setFlash('saveSuccess', 'Данные успешно сохранены.');
            return $this->redirect('');
        }


        // Вывод
        $realOperationSystem = $this->_resourceReal()->getOperationSystemItems();
        $realOperationSystemBit = $this->_resourceReal()->getOperationSystemBitItems();;
        $sourceItems = ParserOperationSystemModel::find()->asArray()->all();

        $realOperationSystemData = ArrayHelper::map($realOperationSystem, 'id', 'name');
        $realOperationSystemBitData = ArrayHelper::map($realOperationSystemBit, 'id', 'name');
        $operationSystemSelectedData = ArrayHelper::map($sourceItems, 'id', 'realOperationSystemId');
        $operationSystemBitSelectedData = ArrayHelper::map($sourceItems, 'id', 'realOperationSystemBitId');


        return $this->render('@parser-view/assoc/operation-system.twig', [
            'sectionName' => 'Операционные системы и разрядность',
            'sourceItems' => $sourceItems,
            'realOperationSystemData' => $realOperationSystemData,
            'realOperationSystemBitData' => $realOperationSystemBitData,
            'operationSystemSelectedData' => $operationSystemSelectedData,
            'operationSystemBitSelectedData' => $operationSystemBitSelectedData,
        ]);
    }
}

