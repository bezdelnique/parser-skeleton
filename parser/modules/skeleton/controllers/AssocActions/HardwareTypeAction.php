<?php
/**
 * Created by PhpStorm.
 * User: heman
 * Date: 26.01.2018
 * Time: 12:58
 */

namespace parser\modules\skeleton\controllers\AssocActions;


use parser\AbstractAssocAction;
use parser\modules\skeleton\entities\HardwareTree\ParserHardwareTreeModel;
use yii\helpers\ArrayHelper;


class HardwareTypeAction extends AbstractAssocAction
{
    public function run()
    {
        if (\Yii::$app->request->isPost) {
            $assocHardwareType = \Yii::$app->request->post('assocHardwareType');
            foreach ($assocHardwareType as $sourceId => $realHardwareTypeId) {
                if (empty($realHardwareTypeId)) {
                    continue;
                }

                ParserHardwareTreeModel::updateAll([
                    'realHardwareTypeId' => $realHardwareTypeId,
                ], ['id' => $sourceId, 'parentId' => 0]);
            }

            \Yii::$app->getSession()->setFlash('saveSuccess', 'Данные успешно сохранены.');
            return $this->redirect('');
        }


        // Вывод
        $realHardwareTypes = $this->_resourceReal()->getHardwareTypeItems();
        $sourceHardwareTypes = ParserHardwareTreeModel::find()->andWhere(['parentId' => 0])->asArray()->all();
        $realHardwareTypeData = ArrayHelper::map($realHardwareTypes, 'id', 'name');
        $sourceSelectedData = ArrayHelper::map($sourceHardwareTypes, 'id', 'realHardwareTypeId');

        return $this->render('@parser-view/assoc/hardware-type.twig', [
            'sectionName' => 'Типы оборудования',
            'realHardwareTypeData' => $realHardwareTypeData,
            'sourceItems' => $sourceHardwareTypes,
            'sourceSelectedData' => $sourceSelectedData,
        ]);
    }
}

