<?php
/**
 * Created by PhpStorm.
 * User: heman
 * Date: 26.01.2018
 * Time: 15:03
 */

namespace appbackend\controllers\Assoc\Msi;

namespace parser\modules\skeleton\controllers\AssocActions;


use parser\AbstractAssocAction;
use parser\components\PagerComponent;
use parser\ExceptionController;
use parser\modules\skeleton\entities\HardwareTree\ParserHardwareTreeModel;
use yii\helpers\ArrayHelper;


class HardwareAction extends AbstractAssocAction
{
    private $_cacheHardwareSource = [];


    public function run()
    {
        $perPage = 50;

        $sourceHardwareTypeId = \Yii::$app->request->get('sourceHardwareTypeId', null);
        $sourceHardwareSeriesId = \Yii::$app->request->get('sourceHardwareSeriesId', null);
        if (is_null($sourceHardwareTypeId) or is_null($sourceHardwareSeriesId)) {
            return $this->redirect('hardware-tree');
        }


        $sourceHardwareType = ParserHardwareTreeModel::findOne([
            'id' => $sourceHardwareTypeId,
            // 'typeId' => ParserHardwareTreeModel::C_typeType,
        ]);
        if (empty($sourceHardwareType)) {
            throw new ExceptionController("sourceHardwareTypeId {$sourceHardwareTypeId} does not have assoc.");
        }
        $realHardwareTypeId = $sourceHardwareType->realHardwareTypeId;
        $sourceHardwareTypeName = $sourceHardwareType->name;

        $sourceHardwareSeries = ParserHardwareTreeModel::findOne([
            'id' => $sourceHardwareSeriesId,
            // 'typeId' => ParserHardwareTreeModel::C_typeSeries,
        ]);
        if (empty($sourceHardwareSeries) or empty($sourceHardwareSeries->realHardwareSeriesId)) {
            throw new ExceptionController("sourceHardwareSeriesId {$sourceHardwareSeriesId} does not have assoc.");
        }
        $sourceHardwareSeriesId = $sourceHardwareSeries->getId();
        $sourceHardwareSeriesName = $sourceHardwareSeries->getName();
        $realHardwareSeriesId = $sourceHardwareSeries->realHardwareSeriesId;


        // total
        $sourceHardwareQuery = ParserHardwareTreeModel::find()->andWhere([
            'parentId' => $sourceHardwareSeriesId,
            // 'typeId' => ParserHardwareTreeModel::C_typeProduct,
        ]);
        $sourceTotal = $sourceHardwareQuery->asArray()->count();

        // pager
        $pages = ceil($sourceTotal / $perPage);
        $page = \Yii::$app->request->get('page');
        $pager = new PagerComponent($pages, $page);
        $pager->setUrl(\Yii::$app->request->pathInfo, \Yii::$app->request->get());
        $offset = 0;
        if ($page > 1) {
            $offset = ($pager->getPage() - 1) * $perPage;
        }

        // source data
        $sourceHardwareItems = $sourceHardwareQuery
            ->offset($offset)
            ->limit($perPage)
            ->indexBy('id')
            ->asArray()
            ->all();


        if (\Yii::$app->request->isPost) {
            $sourceItems = \Yii::$app->request->post('sourceItems');
            $sourceItemsNew = \Yii::$app->request->post('new', []);
            $sourceItemsReset = \Yii::$app->request->post('reset', []);

            $errors = $this->_saveAssoc($sourceItems, $sourceItemsNew, $sourceItemsReset, $sourceHardwareItems, $realHardwareTypeId, $realHardwareSeriesId);

            if (empty($errors)) {
                \Yii::$app->getSession()->setFlash('saveSuccess', 'Данные успешно сохранены.');
            } else {
                \Yii::$app->getSession()->setFlash('saveErrors', 'Возникли ошибки:<br>' . join('<br>', $errors));
            }

            return $this->redirect('');
        }


        // Вывод
        $realHardwareItems = $this->_resourceReal()->getHardwareItems($this->_companyId(), [
            'hardwareTypeId' => $realHardwareTypeId
        ]);
        $realHardwareBySeriesItems = $this->_resourceReal()->getHardwareItems($this->_companyId(), [
            'hardwareTypeId' => $realHardwareTypeId,
            'hardwareSeriesId' => $realHardwareSeriesId,
        ]);


        $realHardwareData = [];
        foreach ($realHardwareItems as $item) {
            $realHardwareData[$item['id']] = "{$item['name']} ({$item['id']})";
        }

        $realHardwareBySeriesData = [];
        foreach ($realHardwareBySeriesItems as $item) {
            $realHardwareBySeriesData[$item['id']] = "{$item['name']} ({$item['id']})";
        }

        $selectedSourceData = ArrayHelper::map($sourceHardwareItems, 'id', 'realHardwareId');


        $assocData = [];
        $matchedRealData = $this->_getMatchedRealData($sourceHardwareItems, ArrayHelper::map($realHardwareItems, 'id', 'name'));
        foreach ($sourceHardwareItems as $item) {
            $sourceId = $item['id'];

            $assocData[$sourceId]['Разное'] = [-1 => 'Оставить без ассоциации'];
            if (!empty($matchedRealData[$sourceId])) {
                $assocData[$sourceId]['Подходящие'] = $matchedRealData[$sourceId];
            }

            if (!empty($realHardwareBySeriesData)) {
                $assocData[$sourceId]['По серии'] = $realHardwareBySeriesData;
            }

            $assocData[$sourceId]['Всё сразу'] = $realHardwareData;
        }

        return $this->render('@parser-view/assoc/hardware.twig', [
            'sectionName' => sprintf('%s, %s', $sourceHardwareTypeName, $sourceHardwareSeriesName),
            'data' => $realHardwareItems,
            'assocData' => $assocData,
            'sourceData' => $sourceHardwareItems,
            'sourceSelectedData' => $selectedSourceData,
            // 'total' => $total,
            'sourceTotal' => $sourceTotal,
            'realHardwareTypeId' => $realHardwareTypeId,
            'sourceHardwareTypeId' => $sourceHardwareTypeId,
            'realHardwareSeriesId' => $realHardwareSeriesId,
            'sourceHardwareSeriesId' => $sourceHardwareSeriesId,
            'pager' => $pager,
        ]);
    }


    protected function _getMatchedRealData(array $sourceHardwareItems, array $hardwareAssoc): array
    {
        $matchAssocDataBySource = [];
        foreach ($sourceHardwareItems as $sourceHardwareItem) {
            foreach ($hardwareAssoc as $id => $name) {
                $nameOrigin = "{$name} ({$id})";

                $sourceId = $sourceHardwareItem['id'];
                $levenshtein = levenshtein($sourceHardwareItem['name'], $name);
                if ($levenshtein > 2) {
                    continue;
                }
                // echo "{$sourceHardwareItem['name']} => {$name} : {$levenshtein} <br>\n";
                $matchAssocDataBySource[$sourceId][$id] = $nameOrigin;
            }
        }

        return $matchAssocDataBySource;
    }


    private function _saveAssoc(array $sourceItems, array $sourceItemsNew, array $sourceItemsReset, array $sourceHardwareItems, $realHardwareTypeId, $realHardwareSeriesId): array
    {
        $errors = [];

        // Assoc
        foreach ($sourceItems as $sourceHardwareId => $realHardwareId) {
            if (empty($realHardwareId) or $realHardwareId == -1) {
                continue;
            }

            try {
                $this->_resourceReal()->assocHardware($realHardwareId, $realHardwareSeriesId, $sourceHardwareId);

                $sourceHardware = $this->_findHardwareSource($sourceHardwareId);
                $sourceHardware->realHardwareId = $realHardwareId;
                $sourceHardware->saveOrRaiseException();
            } catch (\Exception $e) {
                $errors[] = $e->getMessage();
            }
        }

        // Create
        foreach ($sourceItemsNew as $sourceHardwareId) {
            try {
                $realHardwareItem = $this->_resourceReal()->createHardware([
                    'name' => $sourceHardwareItems[$sourceHardwareId]['name'],
                    'companyId' => $this->_companyId(),
                    'hardwareTypeId' => $realHardwareTypeId,
                    'hardwareSeriesId' => $realHardwareSeriesId,
                    'sourceHardwareId' => $sourceHardwareId,
                ]);

                $sourceHardware = $this->_findHardwareSource($sourceHardwareId);
                $sourceHardware->realHardwareId = $realHardwareItem['id'];
                $sourceHardware->saveOrRaiseException();
            } catch (\Exception $e) {
                $errors[] = $e->getMessage();
            }
        }

        // Reset assoc
        foreach ($sourceItemsReset as $sourceHardwareId => $realHardwareId) {
            $sourceHardwareId = (int)$sourceHardwareId;

            try {
                $this->_resourceReal()->assocHardwareReset($realHardwareId, $sourceHardwareId);

                $sourceHardware = $this->_findHardwareSource($sourceHardwareId);
                $sourceHardware->realHardwareId = 0;
                $sourceHardware->saveOrRaiseException();
            } catch (\Exception $e) {
                $errors[] = $e->getMessage();
            }
        }

        return $errors;
    }


    private function _findHardwareSource(int $sourceHardwareId): ParserHardwareTreeModel
    {
        if (!isset($this->_cacheHardwareSource[$sourceHardwareId]) or is_null($this->_cacheHardwareSource[$sourceHardwareId])) {
            $sourceHardware = ParserHardwareTreeModel::findOne([
                'id' => $sourceHardwareId,
                // 'typeId' => ParserHardwareTreeModel::C_typeProduct,
            ]);
            if (empty($sourceHardware)) {
                throw new ExceptionController("sourceHardwareId with id {$sourceHardwareId} not found");
            }

            $this->_cacheHardwareSource[$sourceHardwareId] = $sourceHardware;
        }

        return $this->_cacheHardwareSource[$sourceHardwareId];
    }
}

