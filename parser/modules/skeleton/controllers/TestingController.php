<?php

namespace parser\modules\skeleton\controllers;

use yii\web\Controller;


class TestingController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}

