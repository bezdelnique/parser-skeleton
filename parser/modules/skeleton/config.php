<?php
return yii\helpers\ArrayHelper::merge(
    require __DIR__ . '/../config.php',
    [
        'params' => [
            'companyId' => 1,
            'companyNick' => 'skeleton',
            'companyName' => 'Skeleton',
        ],
    ]
);

