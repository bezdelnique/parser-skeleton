<?php
/**
 * Created by PhpStorm.
 * User: heman
 * Date: 04.12.2018
 * Time: 18:46
 */

namespace parser\modules\skeleton\helpers;


use parser\ParserHelperException;


class Downloader
{
    // private $_companyNick;
    private $_downloadPath;
    private $_stdoutClosure;


    public function __construct(string $downloadPath, \Closure $stdoutClosure)
    {
        $this->_downloadPath = $downloadPath;
        $this->_stdoutClosure = $stdoutClosure;
    }


    public function getTree(): string
    {
        $url = 'https://www.skeleton.com/en/support';
        $subpath = 'tree';
        // $fileName = $this->_getFileNameByUrl(sprintf('TypeID=%d&ParentID=%d', $typeId, $parentId));
        $fileName = 'tree.html';

        return $this->_getContentFromWebOrCachedByParams($subpath, $fileName, $url);
    }


    public function getProductUrl(int $productId): string
    {
        $url = sprintf('https://www.skeleton.com/rest/support_alias/en/%d', $productId);
        $subpath = 'product-url';
        $fileName = $this->_getFileNameByUrl(sprintf('product-url-%d.json', $productId));

        return $this->_getContentFromWebOrCachedByParams($subpath, $fileName, $url);
    }


    public function getProductPage(int $productId, string $url): string
    {
        $url = sprintf('https://www.skeleton.com%s', $url);
        $subpath = 'product-page';
        $fileName = $this->_getFileNameByUrl(sprintf('product-page-%d.html', $productId));

        return $this->_getContentFromWebOrCachedByParams($subpath, $fileName, $url);
    }


    /*
    public function getByTypeIdParentId(int $typeId, int $parentId)
    {
        $url = sprintf('https://www.skeleton.com/Download/API/lookupValueSearch.aspx?TypeID=%d&ParentID=%d', $typeId, $parentId);
        $subpath = 'tree';
        $fileName = $this->_getFileNameByUrl(sprintf('TypeID=%d&ParentID=%d', $typeId, $parentId));

        return $this->_getContentFromWebOrCachedByParams($subpath, $fileName, $url);
    }

    public function getFileList(int $seriesId, int $productId, int $operationSystemId)
    {
        // psid - seriesId
        // pfid - productId
        $url = sprintf('https://www.skeleton.com/Download/processFind.aspx?psid=%d&pfid=%d&osid=%d&lid=1&whql=&lang=en-us&ctk=0', $seriesId, $productId, $operationSystemId);
        $subpath = 'file-list';
        $fileName = $this->_getFileNameByUrl(sprintf('psid=%d&pfid=%d&osid=%d&lid=1&whql=&lang=en-us&ctk=0', $seriesId, $productId, $operationSystemId));

        return $this->_getContentFromWebOrCachedByParams($subpath, $fileName, $url);
    }


    public function getFilePage(int $fileId)
    {
        $url = sprintf('https://www.skeleton.com/download/driverResults.aspx/%d/en-us', $fileId);
        $subpath = 'file-page';
        $fileName = $this->_getFileNameByUrl(sprintf('fileId-%d', $fileId));

        return $this->_getContentFromWebOrCachedByParams($subpath, $fileName, $url);
    }
    */


    private function _getContentFromWebOrCachedByParams(string $subpath, string $fileName, string $url): string
    {
        $path = $this->_downloadPath . '/' . $subpath;
        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }

        $filePath = $path . '/' . $fileName;

        $this->_verbose(sprintf('get url: %s', $url));
        $this->_verbose(sprintf('cache filePath: %s', $filePath));

        if (empty($this->forceWeb) and file_exists($filePath)) {
            $this->_verbose('=> from cache');
            $content = file_get_contents($filePath);
        } else {
            $this->_verbose('=> from web');
            // [$content, $httpCode] = $this->_getPageProductContentCodeByCategoryIdLevel($categoryId, $productId);
            $content = file_get_contents($url);
            if ($content == false) {
                // throw new ParserHelperException(sprintf('http code: %d', $httpCode));
                throw new ParserHelperException("Unable download page: {$url}");
            }
            file_put_contents($filePath, $content);
        }
        $this->_verbose('');

        return $content;
    }


    private function _getContentByParams(array $params): array
    {
        $csrf = '2b284698dff0b8fdd8a032164c266619';

        $client = new \GuzzleHttp\Client([
            'base_uri' => 'https://us.msi.com',
            'headers' => [
                'Content-Type' => 'application/json, text/javascript, */*; q=0.01',
                'Origin' => 'https://us.msi.com',
                'X-Requested-With' => 'XMLHttpRequest',
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36',
                'Referer' => 'https://us.msi.com/support',
                'Cookie' => sprintf('msi_official_csrf=%s;', $csrf),
                'Accept-Encoding' => 'gzip, deflate, br',
                'Accept-Language' => 'ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7',
            ]
        ]);

        $res = $client->request('POST', '/support/ajax/get-download-info-by-product-id', [
            'form_params' => [
                'category' => $categoryId,
                'product_no' => $productId,
                'msi_official_csrf' => $csrf,
            ]
        ]);

        return [$res->getBody(), $res->getStatusCode()];
    }


    private function _getFileNameByUrl(string $url): string
    {
        $fileName = rtrim($url, '/');
        $fileName = str_replace(['/', ':', '&'], '-', $fileName);
        $fileName = str_replace('?', '=', $fileName);

        return $fileName;
    }


    private function _verbose(string $string)
    {
        $stdoutClosure = $this->_stdoutClosure;
        $stdoutClosure($string);
    }
}

