<?php

namespace parser\modules\skeleton;

use parser\modules\ParserModuleAbstract;


class Module extends ParserModuleAbstract
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'parser\modules\skeleton\controllers';


    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
    }


    public function bootstrap($app)
    {
        \Yii::configure($this, require __DIR__ . '/config.php');
        parent::bootstrap($app);

        // Only console and backend app allowed to route
        if ($app->id == $this->params['app-console']) {
            $this->controllerNamespace = 'parser\modules\skeleton\commands';
        } elseif ($app->id == $this->params['app-backend']) {
            // Register action here
            $rulesData = [
                'assoc' => [
                    'operation-system',
                    'hardware-type',
                    'hardware-tree',
                    'hardware',
                ],
                'api' => [
                    'series',
                    'series-save',
                ],
            ];

            $rules = [];
            $rules[] = [
                'class' => 'yii\web\UrlRule',
                'pattern' => $this->id,
                'route' => $this->id . '/assoc/index'
            ];
            foreach ($rulesData as $controller => $actions) {
                foreach ($actions as $action) {
                    $pattern = $this->id . '/' . $controller . '/' . $action;
                    if ($controller == 'assoc') {
                        $pattern = $this->id . '/' . $action;
                    }

                    $rules[] = [
                        'class' => 'yii\web\UrlRule',
                        'pattern' => $pattern,
                        'route' => $this->id . '/' . $controller . '/' . $action
                    ];
                }
            }

            $app->getUrlManager()->addRules($rules, false);
        }
    }
}

