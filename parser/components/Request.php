<?php
/**
 * Created by PhpStorm.
 * User: heman
 * Date: 08.05.2018
 * Time: 12:28
 */

namespace parser\components;


use yii\base\Component;


class Request extends Component
{
    public function getIntOrNull($name, $defaultValue = null): ?int
    {
        $value = \Yii::$app->request->get($name, $defaultValue);
        if (!is_null($value)) {
            $value = intval($value);
        }
        return $value;
    }


    public function get($name, $defaultValue = null)
    {
        return \Yii::$app->request->get($name, $defaultValue);
    }


    public function getIntOrException($name, $defaultValue = null): ?int
    {
        $value = \Yii::$app->request->get($name, $defaultValue);
        $value = trim($value);
        if (!is_null($value)) {
            $value = intval($value);
            if (!is_numeric($value)) {
                throw new ExceptionComponent('Param is not int.');
            }
        }

        return $value;
    }


    public function postIntOrException($name, $defaultValue = null): ?int
    {
        $value = \Yii::$app->request->post($name, $defaultValue);
        $value = trim($value);
        if (!is_null($value)) {
            if (!is_numeric($value)) {
                throw new ExceptionComponent('Param is not int.');
            }
        }

        return $value;
    }


    public function postOrException($name, $defaultValue = null): string
    {
        $value = \Yii::$app->request->post($name, $defaultValue);
        if (!empty($value)) {
            return (string)$value;
        }

        throw new ExceptionComponent(sprintf('Param %s is null or empty.', $name));
    }


    public function getOrException($name, $defaultValue = null)
    {
        $value = \Yii::$app->request->get($name, $defaultValue);
        if (!empty($value)) {
            return $value;
        }

        throw new ExceptionComponent(sprintf('Param %s is null or empty.', $name));
    }


    public function getArrayOfIntFromStringOrException($name, $defaultValue = null): array
    {
        $value = \Yii::$app->request->get($name, $defaultValue);
        if (!is_null($value)) {
            return array_map('intval', explode(',', $value));
        }

        throw new ExceptionComponent(sprintf('Param %s is null or empty.', $name));
    }


    public function postArrayOfIntFromStringOrException($name, $defaultValue = null): array
    {
        $value = \Yii::$app->request->post($name, $defaultValue);
        if (!is_null($value)) {
            return array_map('intval', explode(',', $value));
        }

        throw new ExceptionComponent(sprintf('Param %s is null or empty.', $name));
    }


    public function getArrayOfStringFromStringOrException($name, $defaultValue = null): array
    {
        $value = \Yii::$app->request->get($name, $defaultValue);
        if (!is_null($value)) {
            return explode(',', $value);
        }

        throw new ExceptionComponent(sprintf('Param %s is null or empty.', $name));
    }


    public function postArrayOfStringFromStringOrException($name, $defaultValue = null): array
    {
        $value = \Yii::$app->request->post($name, $defaultValue);
        if (!is_null($value)) {
            return explode(',', $value);
        }

        throw new ExceptionComponent(sprintf('Param %s is null or empty.', $name));
    }


    public function getCurrentUrl(): string
    {
        return parse_url(\Yii::$app->request->getAbsoluteUrl(), PHP_URL_PATH);
    }


    public function getCurrentQs(): array
    {
        return \Yii::$app->request->get();
    }


    public function getCurrentUrlAndQs(): string
    {
        $qs = '';
        if (!empty($_SERVER['QUERY_STRING'])) {
            $qs = $_SERVER['QUERY_STRING'];
        }

        return parse_url(\Yii::$app->request->getAbsoluteUrl(), PHP_URL_PATH) . (($qs) ? "?{$qs}" : '');
    }


    public function getCurrentHost(): string
    {
        $requestScheme = isset($_SERVER['REQUEST_SCHEME']) ? $_SERVER['REQUEST_SCHEME'] : 'http';
        return $requestScheme . '://' . $_SERVER['HTTP_HOST'];
    }


    public function getUrlPathToAbsolute(string $path): string
    {
        $path = ltrim($path, '/');
        return $this->getCurrentHost() . '/' . $path;
    }
}

