<?php
/**
 * Created by PhpStorm.
 * User: heman
 * Date: 06.12.2018
 * Time: 14:37
 */

namespace parser\components\AssocResourceReal;


use parser\AbstractQuery;
use parser\entities\FakeHardware\FakeHardwareModel;
use parser\entities\FakeHardware\Series\FakeHardwareSeriesModel;
use yii\base\Component;

class AssocResourceRealFake extends Component implements AssocResourceRealInterface
{
    public function getOperationSystemItems(): array
    {
        $items = [['id' => '68', 'name' => 'AIX',], ['id' => '56', 'name' => 'Android',], ['id' => '73', 'name' => 'BlackBerry',], ['id' => '41', 'name' => 'Caldera OpenLinux',], ['id' => '29', 'name' => 'CentOS',], ['id' => '75', 'name' => 'Chrome OS',], ['id' => '51', 'name' => 'Citrix',], ['id' => '8', 'name' => 'Debian',], ['id' => '26', 'name' => 'Fedora',], ['id' => '57', 'name' => 'FreeBSD',], ['id' => '70', 'name' => 'HP ThinPro',], ['id' => '31', 'name' => 'HP-UX',], ['id' => '47', 'name' => 'Linux',], ['id' => '42', 'name' => 'Mac OS 8',], ['id' => '25', 'name' => 'Mac OS 9',], ['id' => '38', 'name' => 'Mandriva',], ['id' => '52', 'name' => 'MS-DOS',], ['id' => '59', 'name' => 'NetWare',], ['id' => '46', 'name' => 'OpenServer',], ['id' => '58', 'name' => 'OpenVMS',], ['id' => '69', 'name' => 'Oracle Linux',], ['id' => '77', 'name' => 'OS X El Capitan 10.11',], ['id' => '11', 'name' => 'OS X Jaguar 10.2',], ['id' => '14', 'name' => 'OS X Leopard 10.5',], ['id' => '28', 'name' => 'OS X Lion 10.7',], ['id' => '49', 'name' => 'OS X Mavericks 10.9',], ['id' => '35', 'name' => 'OS X Mountain Lion 10.8',], ['id' => '10', 'name' => 'OS X Panther 10.3',], ['id' => '20', 'name' => 'OS X Puma 10.1',], ['id' => '81', 'name' => 'OS X Sierra 10.12',], ['id' => '7', 'name' => 'OS X Snow Leopard 10.6',], ['id' => '13', 'name' => 'OS X Tiger 10.4',], ['id' => '61', 'name' => 'OS X Yosemite 10.10',], ['id' => '55', 'name' => 'OS/2',], ['id' => '66', 'name' => 'Palm OS',], ['id' => '80', 'name' => 'Pocket PC',], ['id' => '36', 'name' => 'Red Flag Linux',], ['id' => '9', 'name' => 'Red Hat',], ['id' => '32', 'name' => 'Solaris',], ['id' => '5', 'name' => 'SUSE',], ['id' => '37', 'name' => 'Turbolinux',], ['id' => '34', 'name' => 'Ubuntu',], ['id' => '67', 'name' => 'UnixWare',], ['id' => '39', 'name' => 'Unknown OS',], ['id' => '40', 'name' => 'User Manual',], ['id' => '60', 'name' => 'VMware',], ['id' => '64', 'name' => 'VxWorks',], ['id' => '74', 'name' => 'Windows 10',], ['id' => '15', 'name' => 'Windows 2000',], ['id' => '53', 'name' => 'Windows 3.1',], ['id' => '3', 'name' => 'Windows 7',], ['id' => '33', 'name' => 'Windows 8',], ['id' => '50', 'name' => 'Windows 8.1',], ['id' => '16', 'name' => 'Windows 95',], ['id' => '23', 'name' => 'Windows 98',], ['id' => '24', 'name' => 'Windows CE',], ['id' => '54', 'name' => 'Windows Home Server',], ['id' => '21', 'name' => 'Windows ME',], ['id' => '17', 'name' => 'Windows Mobile',], ['id' => '27', 'name' => 'Windows NT',], ['id' => '78', 'name' => 'Windows PE',], ['id' => '72', 'name' => 'Windows Phone',], ['id' => '18', 'name' => 'Windows Server 2003',], ['id' => '19', 'name' => 'Windows Server 2008',], ['id' => '43', 'name' => 'Windows Server 2012',], ['id' => '79', 'name' => 'Windows Server 2016',], ['id' => '12', 'name' => 'Windows Vista',], ['id' => '1', 'name' => 'Windows XP',],];
        foreach ($items as $i => $item) {
            $items[$i]['name'] = sprintf('%s (fake)', $item['name']);
        }

        return $items;
    }


    public function getOperationSystemBitItems(): array
    {
        $items = [['id' => '1', 'name' => '32',], ['id' => '2', 'name' => '64',],];
        foreach ($items as $i => $item) {
            $items[$i]['name'] = sprintf('%s (fake)', $item['name']);
        }

        return $items;
    }


    public function getHardwareTypeItems(): array
    {
        $items = [['id' => '4', 'name' => 'Audio & Sound',], ['id' => '20', 'name' => 'Cellphones & PDA',], ['id' => '2', 'name' => 'Chipsets',], ['id' => '18', 'name' => 'Desktop & All-in-One',], ['id' => '21', 'name' => 'Digital Camera',], ['id' => '11', 'name' => 'Drives & Storage',], ['id' => '22', 'name' => 'Gaming',], ['id' => '3', 'name' => 'Graphics & Video',], ['id' => '10', 'name' => 'Keyboard & Mouse',], ['id' => '8', 'name' => 'Monitors',], ['id' => '5', 'name' => 'Motherboards',], ['id' => '19', 'name' => 'Multimedia',], ['id' => '6', 'name' => 'Network',], ['id' => '7', 'name' => 'Notebooks',], ['id' => '24', 'name' => 'Optical Devices',], ['id' => '25', 'name' => 'Other',], ['id' => '9', 'name' => 'Printers & Scanners',], ['id' => '14', 'name' => 'Servers',], ['id' => '26', 'name' => 'USB Devices',], ['id' => '27', 'name' => 'Wearable',], ['id' => '23', 'name' => 'Webcams & Security',],];
        foreach ($items as $i => $item) {
            $items[$i]['name'] = sprintf('%s (fake)', $item['name']);
        }

        return $items;

    }


    public function getHardwareSeriesItems(int $companyId, array $params = []): array
    {
        $q = FakeHardwareSeriesModel::find()->asArray();
        if (!empty($params['hardwareTypeId'])) {
            $q->andWhere(['hardwareTypeId' => $params['hardwareTypeId']]);
        }

        return $q->all();
    }


    public function getHardwareItems(int $companyId, array $params = []): array
    {
        $q = FakeHardwareModel::find()->asArray();
        if (!empty($params['hardwareTypeId'])) {
            $q->andWhere(['hardwareTypeId' => $params['hardwareTypeId']]);
        }

        if (!empty($params['hardwareSeriesId'])) {
            $q->andWhere(['hardwareSeriesId' => $params['hardwareSeriesId']]);
        }

        return $q->all();
    }


    public function createHardwareSeries(int $companyId, int $hardwareTypeId, string $name, array $attributes = []): array
    {
        $realHardwareSeries = new FakeHardwareSeriesModel();
        $realHardwareSeries->attributes = [
            'name' => $name,
            'hardwareTypeId' => $hardwareTypeId,
            'companyId' => $companyId,
        ];
        $realHardwareSeries->saveOrRaiseException();
        return $realHardwareSeries->toArray();
    }


    public function getHardwareSeriesOrException(int $companyId, int $hardwareSeriesId): array
    {
        // return $this->_createMockHardwareSeries($hardwareSeriesId);
        $realHardwareSeries = FakeHardwareSeriesModel::findOne(['id' => $hardwareSeriesId]);
        if (empty($realHardwareSeries)) {
            throw new ExceptionAssocResourceReal(sprintf('Entity with id %d not found', $hardwareSeriesId));
        }
        return $realHardwareSeries->toArray();
    }


    public function assocHardware(int $realHardwareId, int $realHardwareSeriesId, int $sourceHardwareId): bool
    {
        // return FakeHardwareModel::assoc($realHardwareId, $realHardwareSeriesId, $sourceHardwareId);
        return true;
    }


    public function assocHardwareReset(int $realHardwareId, int $sourceHardwareId): bool
    {
        // return FakeHardwareModel::assocReset($realHardwareId);
        return true;
    }


    public function createHardware(array $attributes): array
    {
        return FakeHardwareModel::add($attributes)->toArray();
    }


    public function joinHardware(AbstractQuery $q, string $tableName, int $companyId): AbstractQuery
    {
        // $q->innerJoin(['h' => FakeHardwareModel::tableName()], "h.sourceHashedId = {$tableName}.id and h.companyId = {$companyId}");
        // $q->innerJoin(['h' => FakeHardwareModel::tableName()], "h.sourceHashedId = {$tableName}.id");
        $q->innerJoin(['h' => FakeHardwareModel::tableName()], "h.id = {$tableName}.realHardwareId");
        return $q;
    }
}

