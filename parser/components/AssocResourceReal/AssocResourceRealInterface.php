<?php
/**
 * Created by PhpStorm.
 * User: heman
 * Date: 06.12.2018
 * Time: 14:39
 */

namespace parser\components\AssocResourceReal;


use parser\AbstractQuery;

interface AssocResourceRealInterface
{
    public function getOperationSystemItems(): array;


    public function getOperationSystemBitItems(): array;


    public function getHardwareTypeItems(): array;


    public function getHardwareSeriesItems(int $companyId, array $params = []): array;


    public function createHardwareSeries(int $companyId, int $hardwareTypeId, string $name, array $attributes = []): array;


    public function getHardwareSeriesOrException(int $companyId, int $hardwareSeriesId): array;


    public function getHardwareItems(int $companyId, array $params = []): array;


    public function assocHardware(int $realHardwareId, int $realHardwareSeriesId, int $sourceHardwareId): bool;


    public function assocHardwareReset(int $realHardwareId, int $sourceHardwareId): bool;


    public function createHardware(array $attributes): array;


    public function joinHardware(AbstractQuery $q, string $tableName, int $companyId): AbstractQuery;
}

