<?php
/**
 * Created by PhpStorm.
 * User: heman
 * Date: 12.12.2018
 * Time: 17:21
 */

namespace parser\helpers\Sys;

use \yii\helpers\ArrayHelper as YiiArrayHelper;


class ArrayHelper extends \bezdelnique\yii2app\helpers\Sys\ArrayHelper
{
    static function parentBy(array $objects, string $prop): array
    {
        $data = [];
        foreach ($objects as $object) {
            if (is_object($object)) {
                $data[$object->{$prop}][] = $object;
            } else {
                $data[$object[$prop]][] = $object;
            }
        }

        return $data;
    }


    static function map(array $items, $from = null, $to = null, $group = null): array
    {
        if (is_null($from)) {
            $data = [];
            foreach ($items as $item) {
                $data[$item] = $item;
            }

            return $data;
        }

        return YiiArrayHelper::map($items, $from, $to, $group);
    }
}

