<?php
/**
 * Created by PhpStorm.
 * User: heman
 * Date: 26.01.2018
 * Time: 14:20
 */

namespace parser;


use parser\components\AssocResourceReal\AssocResourceRealInterface;
use yii\base\Action;
use yii\helpers\Console;


/**
 * Simplify move methods from controller to extracted action
 */
class AbstractConsoleAction extends Action
{
    public function stdout(string $string, $a = null)
    {
        $this->controller->stdout($string, $a);
    }


    public function stdoutnl(string $string = '', $a = null)
    {
        $this->stdout($string . PHP_EOL, $a);
    }


    public function error(string $string)
    {
        $this->stdout($string . PHP_EOL, Console::FG_RED);
    }


    public function warn(string $string)
    {
        $this->stdout($string . PHP_EOL, Console::FG_YELLOW);
    }


    public function info(string $string)
    {
        $this->stdout($string . PHP_EOL, Console::FG_GREEN);
    }


    public function line(string $string)
    {
        $this->stdout($string . PHP_EOL);
    }


    public function verbose(string $string)
    {
        $this->stdout($string . PHP_EOL, Console::FG_CYAN);
    }


    public function header(string $string)
    {
        $this->stdout($string . PHP_EOL, Console::BOLD);
    }


    /**
     * @return AbstractConsoleController
     */
    public function _controller()
    {
        return $this->controller;
    }


    protected function _resourceReal(): AssocResourceRealInterface
    {
        return \Yii::$app->assocResourceReal;
    }
}

