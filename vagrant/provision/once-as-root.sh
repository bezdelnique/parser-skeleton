#!/usr/bin/env bash

source /app/vagrant/provision/common.sh

#== Import script args ==

timezone=$(echo "$1")

#== Provision script ==
info "Provision-script user: `whoami`"
export DEBIAN_FRONTEND=noninteractive

info "Configure timezone"
timedatectl set-timezone ${timezone} --no-ask-password

#Prepare root password for MariaDB
debconf-set-selections <<< "mariadb-server mysql-server/root_password password \"''\""
debconf-set-selections <<< "mariadb-server mysql-server/root_password_again password \"''\""

#MariaDB repo
apt-key adv --recv-keys --keyserver hkp://keyserver.ubuntu.com:80 0xF1656F24C74CD1D8 > /dev/null 2>&1
add-apt-repository -y "deb [arch=amd64,i386,ppc64el] http://mirror.timeweb.ru/mariadb/repo/10.2/ubuntu xenial main"

#PHP 7.2 repo
add-apt-repository -y ppa:ondrej/php

#Midnight commander
apt-get install -y -qq mc > /dev/null 2>&1

#Update / Upgrade
apt-get update -qq > /dev/null 2>&1
#apt-get upgrade -qq -y

info "Installing PHP 7.2 and modules"
apt-get install -y -qq php7.2-curl php7.2-cli php7.2-intl php7.2-mysqlnd php7.2-gd php7.2-fpm php7.2-mbstring php7.2-xml php7.2-tidy php7.2-zip unzip redis-server php.xdebug > /dev/null

info "Installing Nginx"
apt-get install -y -qq nginx > /dev/null 2>&1

info "Installing MariaDB Server"
apt-get install -y -qq mariadb-server > /dev/null
cp /app/vagrant/mysql/my.cnf /etc/mysql/my.cnf

info "Configure PHP-FPM"
sed -i 's/user = www-data/user = vagrant/g' /etc/php/7.2/fpm/pool.d/www.conf
sed -i 's/group = www-data/group = vagrant/g' /etc/php/7.2/fpm/pool.d/www.conf
sed -i 's/owner = www-data/owner = vagrant/g' /etc/php/7.2/fpm/pool.d/www.conf

info "Configure PHP XDEBUG"
# http://yutaf.github.io/xdebug-remote-cli-debugging-with-vagrant-and-phpstorm/
# export XDEBUG_CONFIG="serverName=vagrant idekey=PHPSTORM remote_host=10.0.2.2 remote_port=9000"
cat << EOF > /etc/php/7.2/mods-available/xdebug.ini
zend_extension=xdebug.so

xdebug.remote_enable=true
xdebug.remote_connect_back=true
xdebug.remote_autostart=on
xdebug.remote_enable=on
xdebug.remote_handler="dbgp"
xdebug.remote_host="10.0.2.2"
xdebug.remote_port=9000
xdebug.remote_mode=req
xdebug.idekey="PHPSTORM"

EOF

info "Configure NGINX"
sed -i 's/user www-data/user vagrant/g' /etc/nginx/nginx.conf

info "Enabling site configuration"
ln -s /app/vagrant/nginx/app.conf /etc/nginx/sites-enabled/app.conf

# info "Installing ElasticSearch 6.0"
# apt-get -y -qq install default-jre > /dev/null 2>&1
# wget -q https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-6.0.0.deb
# wget -q https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-6.0.0.deb.sha512
# shasum -a 512 -c elasticsearch-6.0.0.deb.sha512
# sudo dpkg -i elasticsearch-6.0.0.deb > /dev/null

info "Installing composer"
curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer > /dev/null 2>&1

