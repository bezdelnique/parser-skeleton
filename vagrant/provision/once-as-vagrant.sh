#!/usr/bin/env bash

source /app/vagrant/provision/common.sh

#== Import script args ==

github_token=$(echo "$1")

#== Provision script ==

info "Provision-script user: `whoami`"

info "Configure composer"
composer -q config --global github-oauth.github.com ${github_token} > /dev/null 2>&1

info "Install plugins for composer"
composer global require "fxp/composer-asset-plugin:^1.3.1" --no-progress > /dev/null 2>&1

info "Install project dependencies"
cd /app
composer -q --no-progress --prefer-dist install > /dev/null 2>&1

info "Init project"
./init --env=Development --overwrite=y /dev/null 2>&1
sed -i 's/192.168.83.137/localhost/g' /app/common/config/main-local.php
sed -i 's/192.168.83.137/localhost/g' /app/common/config/test-local.php
sed -i 's/192.168.83.137/localhost/g' /app/common/config/params-local.php


# info "Init database"
mysql -uroot -f < /app/vagrant/mysql/init.sql > /dev/null 2>&1

#info "Apply migrations"
#./yii migrate --interactive=0
#./yii_test migrate --interactive=0
./yii migrate --migrationPath=@console/migrations --interactive=0
./yii migrate --migrationPath=@parser/modules/migrations --interactive=0


# info "Import skeleton database"
# mysql -uroot -Dparser -f < /app/vagrant/mysql/parser_skeleton.sql > /dev/null 2>&1


#Create bash-alias 'app' for vagrant user
echo 'alias app="cd /app"' | tee /home/vagrant/.bash_aliases

#Enabling colorized prompt for guest console
sed -i "s/#force_color_prompt=yes/force_color_prompt=yes/" /home/vagrant/.bashrc

